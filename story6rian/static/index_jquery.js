jQuery(document).ready(function($){
    var panels = $('.accordion > dd').hide();
    
    // On click of panel title
    $('.accordion > dt > a').click(function() {
        
        var $this = $(this);
        panels.slideUp();
        $this.parent().next().slideDown();
        return false;
    });
});