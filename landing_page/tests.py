from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status
from .models import MyStatus
from .forms import StatusForm

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from story6rian.settings import BASE_DIR
import time
import os


# Create your tests here.
class TestMyStory6(TestCase):

    def test_story6_url_is_exist(self):
        response = Client().get("")
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_function(self):
        found = resolve("/")
        self.assertEqual(found.func, index)

    def test_model_create_new_status(self):
        MyStatus.objects.create(the_status = "Lagi laper nih:( ")
        
        count_all_status = MyStatus.objects.all().count()
        self.assertEqual(count_all_status, 1)

        response = Client().get("/")
        self.assertContains(response, "Lagi laper nih:( ")
    
    def test_contains_whats_up(self):
        response = Client().get("/")
        self.assertContains(response, "Hello, What's Up?")

    def test_form_validation_for_blank_items(self):
        a_form = StatusForm(data={
            'the_status': '',         
        })
        self.assertFalse(a_form.is_valid())
    

class MyFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome("./chromedriver", chrome_options = chrome_options)

    def test_contains_status_with_functional_test(self):
        driver = self.browser
        driver.get('http://localhost:8000')
        time.sleep(1)
        element = driver.find_element_by_id("id_the_status")
        element.send_keys("Kita coba yaaa")
        time.sleep(1)   
        submit = driver.find_element_by_id("submit_post")
        submit.send_keys(Keys.RETURN)
        assert "Kita coba yaaa" in driver.page_source
        time.sleep(1)

    def tearDown(self):
        self.browser.quit()