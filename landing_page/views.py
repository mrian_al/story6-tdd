from django.shortcuts import render, redirect
from .models import MyStatus
from .forms import StatusForm
# Create your views here.

def index(request):

    all_my_status = MyStatus.objects.all().order_by("time_uploaded")

    a_status_form = StatusForm()
    context = {
        "form" : a_status_form,
        "all_status" : all_my_status
    }

    return render(request, "home.html", context)

def add_status(request):
    
    a_status_form = StatusForm(request.POST or None)
    
    if request.method == "POST":
        if a_status_form.is_valid():
            a_status_form.save()
            return redirect("landing_page:index")
        else:
            a_status_form = StatusForm()

    return redirect("landing_page:index")
