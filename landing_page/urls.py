from django.urls import path, include
from .views import index
from .views import add_status

app_name = "landing_page"

urlpatterns = [
    path("", index, name='index'),
    path("add_status/", add_status, name='add_my_status'),
]
