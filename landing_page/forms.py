from django import forms
from .models import MyStatus

class StatusForm(forms.ModelForm):
    the_status = forms.CharField(
        max_length=300, label= "the_status", 
        widget = forms.Textarea(attrs={
            'title': "post_a_status",
            'name' : "post_a_status",

        })
    )

    class Meta:
        model = MyStatus
        fields = ["the_status"]
