from django.test import TestCase
from django.test import Client
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys

class TestLearnJquery(TestCase):
    response = Client().get("/learnjquery/")

    def test_url_is_exist(self):
        self.assertEqual(self.response.status_code, 200)

    def test_page_contains_some_words(self):
        self.assertContains(self.response, "Aktivitas")

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome("./chromedriver", chrome_options = chrome_options)

    def test_value_can_be_saw(self):
        driver = self.browser
        driver.get('http://localhost:8000/learnjquery')
        time.sleep(1)
        element = driver.find_element_by_id("first_header")
        element.send_keys(Keys.RETURN)
        assert "Aktivitas saya" in driver.page_source
        time.sleep(1)

    def tearDown(self):
        self.browser.quit()

    