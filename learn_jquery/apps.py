from django.apps import AppConfig


class LearnJqueryConfig(AppConfig):
    name = 'learn_jquery'
