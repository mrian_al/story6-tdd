from django.urls import path
from .views import index_jquery

app_name = "learn_jquery"

urlpatterns = [
    path("", index_jquery, name='index_jquery'),
]
