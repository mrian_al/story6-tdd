jQuery(document).ready(function($){
    var panels = $('.accordion > dd').hide();
    
    $('.accordion > dt > a').click(function() {
        
        var self = $(this);
        panels.slideUp();
        self.parent().next().slideDown();
        return false;

    });

    $('#button_theme').click(function() {
        $("html").toggleClass("dark_bg");
        $("body").toggleClass("dark_bg");
        $(".value").toggleClass("dark_whitefont");
        $("#button_theme").toggleClass("btn-outline-dark btn-outline-primary")
        $(".header").toggleClass("dark_bg_header");
    });

});

