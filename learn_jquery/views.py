from django.shortcuts import render

def index_jquery(request):
    return render(request, 'index.html')
